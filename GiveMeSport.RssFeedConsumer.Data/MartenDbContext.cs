﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Marten;

namespace GiveMeSport.RssFeedConsumer.Data
{
    public class MartenDbContext : Services.IDbContext, IDisposable
    {
        private readonly IDocumentStore _store;
        private readonly IDocumentSession _session;

        public MartenDbContext(string connectionString)
        {
            _store = DocumentStore.For(_ =>
            {
                _.Connection(connectionString);
            });
            _session = _store.LightweightSession();
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _session.Query<T>();
        }

        public void Store<T>(T obj) where T : class
        {
            _session.Store<T>(obj);
        }

        public async Task SaveChanges()
        {
            await _session.SaveChangesAsync();
        }

        public void Dispose()
        {
            _session?.Dispose();
            _store.Dispose();
        }
    }
}
