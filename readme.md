# A simple dotnet core mvc application to display latest 10 articles from the Give Me Sport rss feed.

This has been developed on mac using dotnet core and visual studio code and visual studio community.

It contains the following projects
* Services, for core business functionality
* Services.Tests, for testing Services
* Data, for persistence implementation
* Web, mvc project making use of services, to display articles

I've used MartenDb for persistence, which is a framework that sits on top of postgres (hopefully this is ok). Reasons for using this are:
* no schema required, as it stores as documents using postgres' jsonb support, which allowed for quicker prototyping.
* have recently looked into MartenDb when researching event sourcing and event stores.
* I used docker to run an instance of postgres, to save having to do a full install separately.
* postres password `pass` for `postgres` user, db must be created ahead named `give_me_sport`.

I realise this may make it difficult to run if you haven't got relevant setup, so can demo during face to face.

I've used xUnit for testing, as for one reason, this is default with dotnet core.
Given more time I would write better test coverage for the FeedService and other functionality, but hopefully this demonstrates fundamentals.

I've placed `//todo` comments throughout code, specifying some improvements I would make.

I haven't attempted the auto refresh in the advanced requirements, but one solution for this could be to create a WebAPI endpoint to return json articles since a supplied date, and to create a React or Angular 2+ front end, or to keep it simpler, just use some jQuery or angular 1.x (which doesn't require any build process, and so can be simply referenced as js dependency in the browser).  Then setup an interval to poll this endpoint.
Or a more advanced version might be to setup a websocket or event source for server to push articles when they arrive.