﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GiveMeSport.RssFeedConsumer.Web.Models;
using GiveMeSport.RssFeedConsumer.Services;

namespace GiveMeSport.RssFeedConsumer.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFeedService _feedService;

        public HomeController(IFeedService feedService)
        {
            _feedService = feedService;
        }

        //todo: use of async.
        //todo: use of viewmodels.
        public IActionResult Index()
        {
            var latest = _feedService.GetLatestTenFromDb();
            return View(latest);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
