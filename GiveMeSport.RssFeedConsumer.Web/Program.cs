﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GiveMeSport.RssFeedConsumer.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //todo: not the right place to do this, but have done here for brevity.
            SeedDatabase();

            BuildWebHost(args).Run();
        }

        private static void SeedDatabase()
        {
            //todo: store connection string in config.
            var feedService = new Services.FeedService(new Data.MartenDbContext(
                "host=localhost;database=give_me_sport;password=pass;username=postgres"));
            var latest = feedService.GetLatestFromFeed();
            feedService.Store(latest).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
