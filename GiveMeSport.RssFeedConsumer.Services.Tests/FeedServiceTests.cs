using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace GiveMeSport.RssFeedConsumer.Services.Tests
{
    public class FeedService_GetLatestTenFromDb
    {
        private FeedService _feedService;
        private IEnumerable<Item> _result;

        public void Arrange()
        {
            //arrange
            var dbContextMoq = new Moq.Mock<IDbContext>();
            var items = new List<Item>
            {
                new Item {Id = Guid.NewGuid(), Title = "Article 1", PublishedOn = new DateTime(2018, 03, 01)},
                new Item {Id = Guid.NewGuid(), Title = "Article 2", PublishedOn = new DateTime(2018, 03, 02)},
                new Item {Id = Guid.NewGuid(), Title = "Article 3", PublishedOn = new DateTime(2018, 03, 03)},
                new Item {Id = Guid.NewGuid(), Title = "Article 4", PublishedOn = new DateTime(2018, 03, 04)},
                new Item {Id = Guid.NewGuid(), Title = "Article 5", PublishedOn = new DateTime(2018, 03, 05)},
                new Item {Id = Guid.NewGuid(), Title = "Article 6", PublishedOn = new DateTime(2018, 03, 06)},
                new Item {Id = Guid.NewGuid(), Title = "Article 7", PublishedOn = new DateTime(2018, 03, 07)},
                new Item {Id = Guid.NewGuid(), Title = "Article 8", PublishedOn = new DateTime(2018, 03, 08)},
                new Item {Id = Guid.NewGuid(), Title = "Article 9", PublishedOn = new DateTime(2018, 03, 09)},
                new Item {Id = Guid.NewGuid(), Title = "Article 10", PublishedOn = new DateTime(2018, 03, 10)},
                new Item {Id = Guid.NewGuid(), Title = "Article 11", PublishedOn = new DateTime(2018, 03, 11)},
                new Item {Id = Guid.NewGuid(), Title = "Article 12", PublishedOn = new DateTime(2018, 03, 12)},
            };
            dbContextMoq.Setup(x => x.Query<Item>()).Returns(items.AsQueryable());

            _feedService = new FeedService(dbContextMoq.Object);
        }

        public void Act()
        {
            _result = _feedService.GetLatestTenFromDb();
        }

        [Fact]
        public void gets_expected_count()
        {
            Arrange();

            Act();

            //assert
            Assert.Equal(10, _result.Count());
        }
    }
}
