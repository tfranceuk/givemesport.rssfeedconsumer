using System;

namespace GiveMeSport.RssFeedConsumer.Services
{
    public class Item
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public DateTime PublishedOn { get; set; }
    }
}
