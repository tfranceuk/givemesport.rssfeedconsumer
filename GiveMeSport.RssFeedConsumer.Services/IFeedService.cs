﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GiveMeSport.RssFeedConsumer.Services
{
    public interface IFeedService
    {
        IEnumerable<Item> GetLatestFromFeed();
        Task Store(IEnumerable<Item> items);
        IEnumerable<Item> GetLatestTenFromDb();
    }

    //todo: separate db and feed functionality, as don't belong in this same interface.
}
