using System.Linq;
using System.Threading.Tasks;

namespace GiveMeSport.RssFeedConsumer.Services
{
    public interface IDbContext
    {
        IQueryable<T> Query<T>() where T : class;
        void Store<T>(T obj) where T : class;
        Task SaveChanges();
    }
}
