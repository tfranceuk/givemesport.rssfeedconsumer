using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GiveMeSport.RssFeedConsumer.Services
{
    public class FeedService : IFeedService
    {
        public readonly IDbContext _context;

        public FeedService(IDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Item> GetLatestFromFeed()
        {
            var xdoc = XDocument.Load("http://www.givemesport.com/rss.ashx");

            var items = xdoc.Descendants("item")
                .Select(x => new Item {
                    Title = x.Element("title").Value,
                    Description = x.Element("description").Value,
                    Link = x.Element("link").Value,
                    PublishedOn = DateTime.Parse(x.Element("pubDate").Value),
                });

            return items.ToList();
        }

        public async Task Store(IEnumerable<Item> items)
        {
            //todo: use async version of Max.
            var lastPublishedOn = _context.Query<Item>().Max(x => (DateTime?) x.PublishedOn);
            var toStore = items.Where(x => lastPublishedOn == null || x.PublishedOn > lastPublishedOn);
            toStore.ToList().ForEach(x => _context.Store(x));
            await _context.SaveChanges();
        }

        public IEnumerable<Item> GetLatestTenFromDb()
            => _context.Query<Item>().OrderByDescending(x => x.PublishedOn).Take(10).ToList();
    }
}
